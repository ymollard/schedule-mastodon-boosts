"""
Boost a particular Mastodon status for a particular day

Create the JSON file boosts.json with a static export from Peertube:
grep -Eo 'https://indymotion\.fr/w/[a-zA-Z0-9]{22}' IndyMotion.html |sort|uniq
"""

#!/usr/bin/env python
from sys import exit
from os import environ
from json import load
from json.decoder import JSONDecodeError
from datetime import datetime
from mastodon import Mastodon
from mastodon.errors import MastodonUnauthorizedError, MastodonNotFoundError

today = datetime.now().isoformat().split("T")[0]

try:
    with open("boosts.json") as f:
        boosts = load(f)
    url = boosts[today]
except FileNotFoundError:
    exit("File boosts.json not found but compulsory")
except JSONDecodeError:
    exit("File boosts.json has not a valid JSON format")
except KeyError:
    print("No status found to be boosted today")
    exit(0)

mastodon = Mastodon(
    api_base_url=environ['MASTODON_API_BASE_URL'],
    access_token=environ['MASTODON_ACCESS_TOKEN'],
    lang='fr'
)

try:
    account = mastodon.account_verify_credentials()
    print(f"Connected with Mastodon ID {account.id}")

    s = mastodon.search(url)

    if len(s["statuses"]) > 0:
        result = mastodon.status_reblog(s["statuses"][0]["id"])
        if 'id' in result:
            print("Successfully boosted!")
        else:
            exit("Did not manage to boost")
    else:
        exit(f"No result found for status URL {url}")
except (MastodonUnauthorizedError, MastodonNotFoundError) as e:
    exit(str(e))
